# em-dosbox-base

Base image for em-dosbox.

## Custom shells
Modify src/Makefile.am
if WEBASSEMBLY
dosbox_LDFLAGS+=-s WASM=1 -s FORCE_FILESYSTEM=1 --shell-file ../shells/liretro.html
endif

## troubleshooting
docker run --rm -it --entrypoint bash emscripten/emsdk