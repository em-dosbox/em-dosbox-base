FROM emscripten/emsdk
LABEL maintainer="Daniel Cassiero <daniel.cassiero@gmail.com>"

RUN apt-get update && apt-get install -y \
    python3 \
    python-is-python3 \
	automake \
 && rm -rf /var/lib/apt/lists/*

RUN emcc -v
RUN git clone https://github.com/dcassiero/em-dosbox.git
WORKDIR /src/em-dosbox
RUN ./autogen.sh && emconfigure ./configure && make
